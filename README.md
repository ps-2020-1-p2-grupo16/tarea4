# Tarea 4 Programacion de Sistemas

## Integrantes

* **Katiuska Marín**

## Ejecutar proyecto

Antes de ejecutar el programa es necesario especificar al SO el PATH de la libreria dinamica con los siguientes comandos:
```
$ LD_LIBRARY_PATH=./lib/
$ export LD_LIBRARY_PATH
```

Ahora sí. Para el compilado general se creara dos ejecutables, uno con enlazado dinamico y otro con estatico con el comando make:
```
make
```
Estos ejecutables se guardaran en el carpeta bin


Para el compilado de un solo enlazado, dinamico o estatico son necesarios los siguientes comandos:

Para compilado con libreria estatico se ejecuta de esta forma

```
make estatico
```

Se creará un ejecutable en la carpeta bin/ llamado `estatico`, ahora para ejecutar el programa:

```
./bin/estatico -s 900
```
Y se verá una salida asi:

```
horas  min  seg
0       15    0
```


Mientras que para compilado con libreria dinamica usara los siguiente comandos:
```
make dinamico
```

Se creará un ejecutable en la carpeta bin/ llamado `dinamico`, ahora para ejecutar el programa:

```
./bin/dinamico -s 900
```
Y se verá una salida asi:

```
horas  min  seg
0       15    0
```

Tanto las dos opciones de compilado, estatico y dinamico funcionan con los dos argumentos -d y -s:

```
./bin/estatico -d 900
```

```
./bin/dinamico -d 900
```

Y en ambos se verá una salida asi:

```
años  meses  días
2       6      0
```
```
horas  min  seg
0       15    0
```

Si se envía mas o menos argumentos, o las banderas equivocadas, no se ejecutará el programa.
