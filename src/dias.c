#include <stdio.h>

void convertirFecha(int sec){
	int anio, mes, dias;
	
	anio = (sec / 360);
    mes = (sec - (360 * anio)) / 30;
    dias = (sec - (360 * anio) - (mes * 30));

	printf("años  meses  días\n");
	printf("%d    %4d    %3d\n", anio, mes, dias);
}
