#include <conv.h>

bool sflag=false;
bool dflag=false;

/*
 * Author: Grupo 16
 * 25 Junio 2020
*/
int main(int argc, char *argv[]){
	int c, sec;

	if (argc != 3) {
		printf("No argumentos o multiples argumentos detectados..\n");
		return -1;
	}

	while((c = getopt(argc, argv, "s:d:")) != -1){
		switch (c){
			case 's':
				sscanf(argv[2], "%d", &sec);
				sflag=true;
				break;	
			case 'd':
				sscanf(argv[2], "%d", &sec);
				dflag=true;
				break;
			default :
				printf("Argumentos no validos, saliendo...\n");
				return -1;				
		}
	
	}
	if (sflag == true){
		convertirSegundos(sec);
	}

	if (dflag == true){
		convertirFecha(sec);
	}

	return 0;
}
