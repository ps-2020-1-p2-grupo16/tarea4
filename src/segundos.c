#include <stdio.h>

void convertirSegundos(int sec){
	int horas, minutos, segundos;
        
        horas = (int) (sec / 3600);
        minutos = (int) (sec - (3600 * horas)) / 60;
        segundos = (sec - (3600 * horas) - (minutos * 60));

        printf("horas  min  seg\n");
        printf("%d     %3d  %3d\n", horas, minutos, segundos);
}	
